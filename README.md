﻿# Data Translator

C# console application to convert XML to fixed width text file.  

I never needed the functionality to do the reverse (text to xml) but the code stubs are already there for future developement.

See the UserGuide.rtf for more information.

![screenshot01.png](https://bitbucket.org/repo/yz75Gb/images/2260468954-screenshot01.png)